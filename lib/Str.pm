#!/usr/bin/perl

use strict;
use warnings;

package Str;

use Exporter;

our @EXPORT = qw(group_by count contains count_substrings_by);

sub group_by {
    my $by = shift;
    my $strs = \@{$_[0]};

    my %result = ();
    for my $s (@$strs) {
        my $key = &$by( $s );

        if (~ exists($result{$key})) {
            @{$result{$key}} = ();
        }

        my $arr = \@{$result{$key}};
        push(@$arr, $s);
    }

    return %result;
}

sub count {
    my $str = \shift;
    my $strings = \@{$_[0]};
    my $count = 0;

    for my $s (@$strings) {
        if ($s eq $$str) {
            $count++;
        }
    }

    return $count;
}

sub contains {
    my $str = \shift;
    my $strings = \@{$_[0]};

    for my $s (@$strings) {
        if ( $s eq $$str ) {
            return 1;
        }
    }

    0;
}

sub count_substrings_by {
    my $sub = \shift;
    my $string = \shift;

    my $count = 0;
    my $i = 0;
    while( $i < length($$string) ) {
        if (substr($$string, $i, length($$sub)) eq $$sub ) {
            $count++;
        }

        $i++;
    }

    return $count;
}

sub split {
    my $by = \shift;
    my $str = \shift;
}

1;