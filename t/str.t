use strict;
use warnings;

use Test::Simple tests => 4;

use Str qw( group_by count contains count_substrings_by);

sub len {
    my $by = shift;

    return length( $by ) % 2 == 0;
}

my @arr = (
    "abc", "Abc",
    "Bc", "bc",
    "cde", "Cde"
);

my %result = (
    0 => ["Bc", "bc"],
    1 => ["abc", "Abc", "cde", "Cde"],
);

my $routine = \&len;
ok( Str::group_by($routine, \@arr) == %result );

my @second_arr = (
    "abc", "Abc",
    "abc", "bc",
    "cde", "Cde"
);

my $str = "abc";
my $res = 2;
ok( Str::count($str, \@second_arr) == $res );
ok( Str::contains($str, \@second_arr) == 1 );

my $string = "abcaabccabc";
ok( Str::count_substrings_by($str, $string) == 3);
