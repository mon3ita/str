use strict;
use warnings;
use Module::Build;

my $pkg = Module::Build->new(
    module_name => 'Str',
    license => 'MIT',
    dist_abstract => 'Str',
    dist_author => 'Monika Ilieva',
    dist_version_from => 'lib/Str.pm'
);

$pkg->create_build_script();