# Str

```
1. Clone the package
2. Go to the location of the package
3. Run `Build.PL`
4. Run `Build install`
5. Enjoy :)

```

# Methods

Currently contains the following methods.

```
group_by
    Args: $str
          @arr_with_strings
    Return: %result
count
    Args: $str
          @arr_with_strings
    Return: $count
contains
    Args: $str
          @arr_with_strings
    Return: $exists
count_substrings_by
    Args: $substr
          $string
    Return: $count
```